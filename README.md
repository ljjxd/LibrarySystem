# 图书管理系统（Java + MySQL + Swing）

#### 介绍
近年来，随着社会、经济的发展，和人对书籍的需求的增长，许多图书馆的规模都在不断扩大，图书数量、有关图书的各种信息量、来图书馆的读者也成倍增加。巨大的信息量，使传统的人工式管理方法面临着极大的挑战，按照传统的方式管理相关信息，会导致图书馆管理上的混乱，人力与物力过多浪费，信息的准确性和安全性也禁不起推敲。图书馆混乱的管理方式，会使图书馆的负担过重，影响整个图书馆的运作和控制管理。
好在IT行业和Internet的飞速发展，为图书馆的管理方式带来了革新。一套合理、有效，规范和实用的图书管理系统，对图书资料和用户信息进行集中统一的管理。另一方面，图书馆的客人也可以通过该系统查询书籍，自助借书还书，节省了大量的人力物力，实现了信息的自动化处理，提高了处理的及时性和正确性。

### 页面展示
1. 登录页面

 ![Image text](https://img-blog.csdnimg.cn/20210106235523698.png)

2. 管理员 - 主页面

 ![Image text](https://img-blog.csdnimg.cn/20210106235638950.png)

3. 个人信息页面

 ![Image text](https://img-blog.csdnimg.cn/2021010623571177.png) 

4. 用户管理页面

 ![Image text](https://img-blog.csdnimg.cn/20210107000130926.png) 
 
#### 软件架构
使用Java Swing创建的一个图书管理系统，基于[https://gitee.com/minuy/BlueRabbitLibrarySystem](https://gitee.com/minuy/BlueRabbitLibrarySystem)项目二次开发，后续功能还需实现


#### 使用说明

[https://blog.csdn.net/qq_35132089/article/details/112300336](https://blog.csdn.net/qq_35132089/article/details/112300336)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
