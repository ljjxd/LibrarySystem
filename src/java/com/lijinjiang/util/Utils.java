package com.lijinjiang.util;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

public class Utils {

    // 设置统一Logo
    public static void setLogo(JFrame frame){
        // 定义图片路径
        String logoPath = "images/logo.png";
        // 获取该图片
        Image logo = new ImageIcon(ClassLoader.getSystemResource(logoPath)/*图片的路径*/).getImage();
        // 设置图标
        frame.setIconImage(logo);
    }

    // 校验输入框是否为空，是的话提示信息，边框变红；不是的话，将边框设置为默认
    /**
     * @param field     输入框
     * @param message   如果为空的提示信息
     */
    public static void checkFieldNull(JTextField field, String message) {
        String text = field.getText();
        if (text == null || text.length() ==0) {
            field.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.RED)); // 红色
            JOptionPane.showMessageDialog(null, message, "警告", JOptionPane.WARNING_MESSAGE);
            throw new NullPointerException(message);
        } else {
            field.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED)); // 默认边框
        }
    }
}
