package com.lijinjiang.view;

import com.lijinjiang.entity.User;
import com.lijinjiang.listener.ChangePwdView_Listener;

import javax.swing.*;
import java.awt.*;

/**
 * 个人详情页面的修改密码界面
 * 修改自己密码时，needOldPwd为true
 */
@SuppressWarnings("serial")
public class ChangePwdView extends JDialog {
    private JPanel changePwdViewPanel;
    private JLabel originalPwdLabel; // 原密码
    private JLabel newPwdLabel; // 新密码
    private JLabel confirmPwdLabel; // 确认密码
    private MyPasswordField originalPwd; // 原密码输入框
    private MyPasswordField newPwd; // 新密码输入框
    private MyPasswordField confirmPwd; // 确认密码输入框
    User user;
    // 保存按钮
    private JButton savePwdBtn; // 保存
    // 取消按钮
    private JButton closeBtn;

    // 是否需要输入旧密码 - 管理员重置其他用户密码不需要旧密码
    boolean needOldPwd;

    /**
     * @param parentView    父级窗口
     * @param user          原密码用户
     * @param needOldPwd    是否需要旧密码
     */
    public ChangePwdView(JDialog parentView, User user, boolean needOldPwd) {
        super(parentView, true);
        // 实例化一个面板组件
        changePwdViewPanel = new JPanel();
        this.user = user;
        this.needOldPwd = needOldPwd;

        originalPwdLabel = new JLabel("原密码：");
        newPwdLabel = new JLabel("新密码：");
        confirmPwdLabel = new JLabel("确认密码：");
        savePwdBtn = new JButton("保存");
        closeBtn = new JButton("取消");
        originalPwd = new MyPasswordField();
        originalPwd.setPlaceholder("       请输入原密码");
        newPwd = new MyPasswordField();
        newPwd.setPlaceholder("       请输入新密码");
        confirmPwd = new MyPasswordField();
        confirmPwd.setPlaceholder("       请确认新密码");

        // 需要输入旧密码
        if (needOldPwd) {
            // 旧密码标签与输入框
            originalPwdLabel.setFont(new Font("微软雅黑", 0, 17));
            originalPwdLabel.setBounds(60, 38, 100, 20);
            originalPwd.setBounds(150, 35, 180, 30);
            // 如果如要输入旧密码，则将该标签和输入框添加进来，否则不添加
            changePwdViewPanel.add(originalPwdLabel);
            changePwdViewPanel.add(originalPwd);

            // 新密码标签与输入框
            newPwdLabel.setFont(new Font("微软雅黑", 0, 17));
            newPwdLabel.setBounds(60, 88, 100, 20);
            newPwd.setBounds(150, 85, 180, 30);

            // 确认新密码标签与输入框
            confirmPwdLabel.setFont(new Font("微软雅黑", 0, 17));
            confirmPwdLabel.setBounds(60, 138, 100, 20);
            confirmPwd.setBounds(150, 135, 180, 30);

            // 保存和取消按钮
            savePwdBtn.setBounds(60, 200, 80, 25);
            closeBtn.setBounds(250, 200, 80, 25);

            // 设置大小
            this.setSize(400, 300);
        } else { // 不需要输入旧密码
            // 新密码标签与输入框
            newPwdLabel.setFont(new Font("微软雅黑", 0, 17));
            newPwdLabel.setBounds(60, 38, 100, 20);
            newPwd.setBounds(150, 35, 180, 30);

            // 确认新密码标签与输入框
            confirmPwdLabel.setFont(new Font("微软雅黑", 0, 17));
            confirmPwdLabel.setBounds(60, 88, 100, 20);
            confirmPwd.setBounds(150, 85, 180, 30);

            // 保存和取消按钮
            savePwdBtn.setBounds(60, 150, 80, 25);
            closeBtn.setBounds(250, 150, 80, 25);

            // 设置大小
            this.setSize(400, 250);
        }

        changePwdViewPanel.setLayout(null);
        changePwdViewPanel.setFocusable(true);

        // 添加监听
        ChangePwdView_Listener listen = new ChangePwdView_Listener(this, user);

        savePwdBtn.addActionListener(listen);
        closeBtn.addActionListener(listen);

        // 添加其他组件
        changePwdViewPanel.add(newPwdLabel);
        changePwdViewPanel.add(newPwd);
        changePwdViewPanel.add(confirmPwdLabel);
        changePwdViewPanel.add(confirmPwd);
        changePwdViewPanel.add(savePwdBtn);
        changePwdViewPanel.add(closeBtn);
        // 去掉按钮文字周围的焦点框
        savePwdBtn.setFocusPainted(false);
        closeBtn.setFocusPainted(false);

        this.add(changePwdViewPanel);
        // 设置标题
        this.setTitle("修改密码  ");
        // 设置默认关闭操作
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        // 设置居中显示
        CenterView.CenterByWindow(this);
        // 设置可见
        this.setVisible(true);
    }

    // 获取旧密码的输入框
    public MyPasswordField getOriginalPwd() {
        return originalPwd;
    }

    // 获取新密码的输入框
    public MyPasswordField getNewPwd() {
        return newPwd;
    }

    // 获取确认新密码的输入框
    public MyPasswordField getConfirmPwd() {
        return confirmPwd;
    }

    // 获取是否需要输入旧密码
    public boolean getNeedOldPwd() {
        return needOldPwd;
    }

    // 在按钮监听中获取保存按钮
    public JButton getSavePwdBtn() {
        return savePwdBtn;
    }

    // 在按钮监听中获取取消按钮
    public JButton getCloseBtn() {
        return closeBtn;
    }

}