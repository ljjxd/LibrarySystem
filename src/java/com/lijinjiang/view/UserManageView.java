package com.lijinjiang.view;

import com.lijinjiang.entity.User;
import com.lijinjiang.listener.UserManageView_MouseListener;
import com.lijinjiang.service.UserService;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.Vector;

/**
 * 用户信息管理界面
 */
@SuppressWarnings("serial")
public class UserManageView extends JDialog {
    JPanel main;
    private int WindowsHeight;
    private int windowsWidth;
    private int buttonSize = 50;

    private JPanel buttonView;
    private JPanel toolView;
    private JPanel tableView;
    private JPanel InfoView;

    private JLabel addBtn; // 新增按钮
    private JLabel modifyBtn; // 修改按钮
    private JLabel deleteBtn; // 删除按钮
    private JLabel resetPwdBtn; // 重置密码按钮
    private JLabel closeBtn; // 关闭按钮

    private JLabel searchWayLabel;
    private JLabel searchInfoLabel;
    @SuppressWarnings("rawtypes")
    private JComboBox searchWay;
    private JTextField searchInfo; // 输入的查询信息
    private JLabel searchBtn; // 查询按钮

    private MyTable tableDataView;
    private DefaultTableModel tableModel; // 默认表格模型
    private JScrollPane snpView;

    MainView mv;


    @SuppressWarnings("rawtypes")
    public UserManageView(MainView mv) {
        super(mv, "用户信息管理", true);
        this.mv = mv;
        WindowsHeight = mv.getHeight();
        windowsWidth = mv.getWidth();
        main = new JPanel();
        buttonView = new JPanel();
        addBtn = new JLabel("新增用户", JLabel.CENTER); // 新增按钮
        modifyBtn = new JLabel("修改信息", JLabel.CENTER);
        deleteBtn = new JLabel("用户删除", JLabel.CENTER);
        resetPwdBtn = new JLabel("重置密码", JLabel.CENTER);
        closeBtn = new JLabel("关闭", JLabel.CENTER);
        InfoView = new JPanel();
        toolView = new JPanel();
        searchWayLabel = new JLabel("选择搜索方式");
        searchInfoLabel = new JLabel(" 输入关键词");
        searchWay = new JComboBox();
        searchInfo = new JTextField();
        searchBtn = new JLabel("搜索一下", JLabel.CENTER);
        tableView = new JPanel();
        snpView = new JScrollPane();
        tableDataView = new MyTable(30); // 实例化一个行高为27的MyTable
        Init();
    }


    @SuppressWarnings("unchecked")
    private void Init() {
        // TODO Auto-generated method stub
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setSize(windowsWidth - 100, WindowsHeight - 100);
        CenterView.CenterByWindow(this);
        // 不允许用户调整窗口大小
        this.setResizable(false);
        // 边界布局
        main.setLayout(new BorderLayout());
        // 流水布局
        buttonView.setLayout(new FlowLayout(FlowLayout.LEFT));
        // 边界布局
        InfoView.setLayout(new BorderLayout());
        // 流水布局
        toolView.setLayout(new FlowLayout(FlowLayout.LEFT));
        // 网格布局
        tableView.setLayout(new GridLayout(1, 1));

        // 设置各个按钮大小
        addBtn.setPreferredSize(new Dimension(64, buttonSize));
        modifyBtn.setPreferredSize(new Dimension(64, buttonSize));
        deleteBtn.setPreferredSize(new Dimension(64, buttonSize));
        resetPwdBtn.setPreferredSize(new Dimension(64, buttonSize));
        closeBtn.setPreferredSize(new Dimension(buttonSize, buttonSize));


        addBtn.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        modifyBtn.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        deleteBtn.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        closeBtn.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        resetPwdBtn.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        buttonView.add(addBtn, BorderLayout.CENTER);
        buttonView.add(modifyBtn, BorderLayout.CENTER);
        buttonView.add(deleteBtn, BorderLayout.CENTER);
        buttonView.add(resetPwdBtn, BorderLayout.CENTER);
        buttonView.add(closeBtn, BorderLayout.CENTER);

        //工具栏的处理
        searchWay.addItem("账 号");
        searchWay.addItem("姓 名");
        searchWay.addItem("学 院");
        searchWay.addItem("电 话");
        searchWay.setPreferredSize(new Dimension(60, 29));

        toolView.add(searchWayLabel);
        toolView.add(searchWay);
        toolView.add(searchInfoLabel);
        toolView.add(searchInfo);
        searchInfo.setPreferredSize(new Dimension(120, 29));

        toolView.add(searchBtn, BorderLayout.CENTER);
        // 设置查询按钮边框
        searchBtn.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        // 设置查询按钮大小
        searchBtn.setPreferredSize(new Dimension(80, 30));

        // 获取数据
        refreshTableData();

        snpView.getViewport().add(tableDataView);
        tableView.add(snpView);

        main.add(buttonView, BorderLayout.NORTH);
        main.add(InfoView, BorderLayout.CENTER);

        InfoView.add(toolView, BorderLayout.NORTH);
        InfoView.add(snpView, BorderLayout.CENTER);

        // 创建监听器
        UserManageView_MouseListener listener = new UserManageView_MouseListener(this);
        // 监听新增事件
        addBtn.addMouseListener(listener);
        // 监听修改事件
        modifyBtn.addMouseListener(listener);
        // 监听删除事件
        deleteBtn.addMouseListener(listener);
        // 监听重置密码事件
        resetPwdBtn.addMouseListener(listener);
        // 监听搜索事件
        searchBtn.addMouseListener(listener);
        // 监听关闭事件
        closeBtn.addMouseListener(listener);
        // 监听表格事件
        tableDataView.addMouseListener(listener);
        main.setFocusable(true); // 设置焦点
        this.add(main);
        this.setVisible(true);
    }


    // 设置表单数据
    @SuppressWarnings({"unchecked", "rawtypes"})
    public void setTableData(List<User> list) {
        int index = 1;
        // TODO Auto-generated method stub
        //1.设置表格中的标题数据集合
        Vector<String> title = new Vector<String>();
        title.add("序  号");
        title.add("账  号");
        title.add("姓  名");
        title.add("性  别");
        title.add("学  院");
        title.add("电  话");
        title.add("管理员");

        //2.设置表格中的数据集合
        Vector<Vector> vec = new Vector<Vector>();
        Vector row = null;
        for (User user : list) {
            row = new Vector();
            row.add(index++); // 序号
            row.add("  " + user.getAccount()); // 账号
            row.add("  " + user.getName()); // 姓名
            // 性别
            if (user.getSex() == 0) {
                row.add("  未知");
            } else if (user.getSex() == 1) {
                row.add("  男");
            } else if (user.getSex() == 2) {
                row.add("  女");
            }
            row.add("  " + user.getCollege()); // 学院
            row.add("  " + user.getPhone()); // 电话
            // 管理员
            if (user.getAdmin()) {
                row.add("  是");
            } else {
                row.add("  否");
            }
            vec.add(row);
            // System.out.println("vec:" + vec);
        }

        // 设置tableModel
        this.tableModel = new DefaultTableModel(vec, title);

        //将tableModel绑定在table上
        this.tableDataView.setModel(tableModel);
        // 自定义列居中和最大宽度
        tableDataView.customColumn("序  号");
    }

    // 获取新增按钮
    public JLabel getAddBtn() {
        return addBtn;
    }

    // 获取关闭按钮
    public JLabel getCloseBtn() {
        return closeBtn;
    }

    // 获取删除按钮
    public JLabel getDeleteBtn() {
        return deleteBtn;
    }

    // 获取修改按钮
    public JLabel getModifyBtn() {
        return modifyBtn;
    }


    //更新表单数据
    public void refreshTableData() {
        // TODO Auto-generated method stub
        // 获取数据
        UserService userService = new UserService();
        List<User> list = userService.getAllUser();
        // System.out.println("list:" + list);
        // 设置数据
        setTableData(list);
    }

    // 获取点击查询的按钮
    public JLabel getSearchBtn() {
        // TODO Auto-generated method stub
        return this.searchBtn;
    }

    // 获取输入的查询信息
    public JTextField getSearchInfo() {
        return this.searchInfo;
    }

    // 获取重置密码组件
    public JLabel getResetPwdBtn() {
        return resetPwdBtn;
    }

    // 获取查询方式组件

    @SuppressWarnings("rawtypes")
    public JComboBox getSearchWay() {
        return this.searchWay;
    }

    // 获取表单
    public MyTable getTableDataView() {
        return tableDataView;
    }

    // 获取当前所在的主页面
    public MainView getMainView() {
        return this.mv;
    }
}
