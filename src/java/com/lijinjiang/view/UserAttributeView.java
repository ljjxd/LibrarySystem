package com.lijinjiang.view;

import com.lijinjiang.entity.User;
import com.lijinjiang.listener.UserAttributeView_ActionListener;
import com.lijinjiang.service.UserService;

import javax.swing.*;
import java.awt.*;

/**
 * 用户信息查看，修改，新增界面
 */
@SuppressWarnings("serial")
public class UserAttributeView extends JDialog {
    private boolean addModel; // 模式，true：添加模式，false：非添加模式-需要加载信息
    private JPanel main; // 主面板
    private JPanel contentView; // 上面的内容面板
    private JPanel functionView; // 下面的操作面板
    private int parentHeight; // 父视图高度
    private int parentWidth; // 父视图宽度

    //用户信息
    private ComboLabelField accountCombo; // 学号
    private ComboLabelField nameCombo; // 姓名
    private ComboLabelField collegeCombo; // 学院
    private ComboLabelField phoneCombo; // 电话

    private JPanel sexPanel; // 性别面板
    private JComboBox<String> sexModel;
    private JLabel sexLabel; // 性别标签

    private JCheckBox isAdminBox; // 是否管理员
    private JPanel isAdminPanel; // 管理员面板

    private JButton saveBtn; // 保存按钮，用于添加或者修改信息
    private JButton cancelBtn; // 取消按钮

    private UserManageView userManageView;
    boolean readOnly; // 是否只读
    private String account = null; // 账号

    String[] sexList = {"未知", "男", "女"}; // 性别框

    /**
     * 用户信息详细页面
     *
     * @param userManageView 用户信息管理器，父视图
     * @param title          当前的详情页面标题
     * @param account        账号，有账号根据账号加载个人信息，没有为null的话就不加载
     * @param readOnly       是否只读
     */
    public UserAttributeView(UserManageView userManageView, String title, String account, boolean readOnly) {
        super(userManageView, title, true);
        this.userManageView = userManageView;
        this.readOnly = readOnly;

        // 如果学号为空，模式设置为true，添加模式，不加载信息
        if (account == null || account.length() == 0) {
            addModel = true;
            account = "";
        } else {
            // 如果学号不为空，模式设置为false，非添加模式，加载信息
            addModel = false;
            this.account = account;
        }

        //获得父视图的大小
        parentHeight = userManageView.getHeight();
        parentWidth = userManageView.getWidth();

        //界面部分
        main = new JPanel();
        functionView = new JPanel();
        contentView = new JPanel();
        sexPanel = new JPanel();
        isAdminPanel = new JPanel();

        // 输入框长度
        int width = 20;

        // 用户信息
        accountCombo = new ComboLabelField("账号：", readOnly, width); // 账号
        nameCombo = new ComboLabelField("姓名：", readOnly, width); // 姓名
        collegeCombo = new ComboLabelField("学院：", readOnly, width); // 学院
        phoneCombo = new ComboLabelField("电话：", readOnly, width); // 电话
        sexModel = new JComboBox<String>();
        sexLabel = new JLabel("性别："); // 性别
        sexModel.setModel(new DefaultComboBoxModel<String>(sexList));
        isAdminBox = new JCheckBox("管理员"); // 管理员
        if (readOnly) {
            // 禁止更改性别和是否管理员
            sexModel.setEnabled(false);
            isAdminBox.setEnabled(false);
        }


        saveBtn = new JButton("确定");
        cancelBtn = new JButton("取消");
        // 去掉按钮文字周围的焦点框
        saveBtn.setFocusPainted(false);
        cancelBtn.setFocusPainted(false);
        isAdminBox.setFocusPainted(false);

        // 不是添加模式
        if (!addModel) {
            // 不为空，是修改信息，在这里载入初始化信息
            System.out.println("加载账号信息！");
            loader(account);
            saveBtn = new JButton("更新");
            // 禁止更改学号
            accountCombo.getValueField().setEditable(false);
        }

        Init();
    }

    /**
     * 加载用户信息函数，设置打开时的初始值
     *
     * @param account 用户的账号
     */
    private void loader(String account) {
        User user = null;
        // 获取用户信息
        user = new UserService().getUserByAccount(account);
        // 设置用户信息
        accountCombo.setValueText(user.getAccount()); // 学号
        nameCombo.setValueText(user.getName()); // 姓名
        collegeCombo.setValueText(user.getCollege());//学院
        // 性别
        if (user.getSex() == 0) {
            sexModel.setSelectedIndex(0);
        } else if (user.getSex() == 1) {
            sexModel.setSelectedIndex(1);
        } else if (user.getSex() == 2) {
            sexModel.setSelectedIndex(2);
        }
        phoneCombo.setValueText(user.getPhone()); // 电话
        // 管理员
        if (user.getAdmin()) {
            isAdminBox.setSelected(true);
        }
    }

    /**
     * 界面初始化函数，内部调用，设置布局什么的
     */
    private void Init() {
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setSize(parentWidth - 550, parentHeight - 383);
        CenterView.CenterByWindow(this);
        // 不允许用户调整窗口大小

        // 设置布局
        main.setLayout(new BorderLayout());
        contentView.setLayout(new GridLayout(3, 2));
        functionView.setLayout(new FlowLayout(FlowLayout.RIGHT));

        // 配置内容视图边框
        contentView.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY));

        // 用户信息
        contentView.add(accountCombo); // 账号
        contentView.add(nameCombo); // 姓名
        contentView.add(collegeCombo);// 学院
        sexPanel.setLayout(new FlowLayout());
        sexPanel.add(sexLabel);
        sexPanel.add(sexModel);
        contentView.add(sexPanel); // 性别
        sexModel.setPreferredSize(new Dimension(126, 28));
        contentView.add(phoneCombo); // 电话
        isAdminPanel.setLayout(new FlowLayout());
        isAdminPanel.add(isAdminBox);
        isAdminBox.setPreferredSize(new Dimension(163, 27));
        contentView.add(isAdminPanel); // 是否管理员


        // 添加监听事件
        UserAttributeView_ActionListener listener = new UserAttributeView_ActionListener(this, userManageView);
        cancelBtn.addActionListener(listener);
        saveBtn.addActionListener(listener);

        // 如果不是只读
        if (!readOnly) {
            functionView.add(saveBtn);
            saveBtn.setPreferredSize(new Dimension(60,25));
        }
        functionView.add(cancelBtn);
        main.add(functionView, BorderLayout.SOUTH);
        main.add(contentView, BorderLayout.CENTER);
        cancelBtn.setPreferredSize(new Dimension(60,25));
        this.add(main);
        this.setVisible(true);
    }

    // 模式，true：添加模式，false：非添加模式-需要加载信息
    public boolean isAddModel() {
        return addModel;
    }

    /**
     * @return 确定或更新按钮
     */
    public JButton getSaveBtn() {
        return this.saveBtn;
    }

    /**
     * @return 取消按钮
     */
    public JButton getCancelBtn() {
        return this.cancelBtn;
    }

    /**
     * @return 用户学号组合类
     */
    public ComboLabelField getAccountCombo() {
        return accountCombo;
    }

    /**
     * @return 用户名字组合类
     */
    public ComboLabelField getNameCombo() {
        return nameCombo;
    }

    /**
     * @return 用户学院组合类
     */
    public ComboLabelField getCollegeCombo() {
        return collegeCombo;
    }


    /**
     * @return 用户电话组合类
     */
    public ComboLabelField getPhoneCombo() {
        return phoneCombo;
    }

    /**
     * 提供的控件接口函数
     *
     * @return 用户性别下拉框
     */
    public JComboBox<String> getSexModel() {
        return sexModel;
    }

    /**
     * @return 用户是否管理员
     */
    public JCheckBox getIsAdminBox() {
        return isAdminBox;
    }

    /**
     * 获取用户载入账号
     *
     * @return 刚刚新建的时候的用户账号，没有的话是null
     */
    public String getAccount() {
        return account;
    }
}
