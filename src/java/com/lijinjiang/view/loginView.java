package com.lijinjiang.view;

import com.lijinjiang.listener.LoginView_helpButton_ActionListener;
import com.lijinjiang.listener.LoginView_loginButton_ActionListener;
import com.lijinjiang.util.Utils;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;

import static com.lijinjiang.util.Utils.setLogo;

/**
 * 登录界面
 */
@SuppressWarnings("serial")
public class loginView extends JFrame {
    // 定义一个“画布”，相当于是画控件的白纸
    private JPanel loginWin;
    // 定义一些标签，用于显示文字
    private JLabel loginTitle; // 标题“图书管理系统”
    private JLabel loginName; // 账号
    private JLabel loginPWD; // 密码
    // 定义了一些按钮，登录和帮助按钮
    private JButton loginButton; // 登录
    private JButton helpButton; // 帮助
    // 定义一个下拉列表，用于选择历史登录账号
    private MyComboBox<String> usernameField; // 用户输入框
    private MyPasswordField passwordField;// 密码输入框

    // 构造函数，一般界面分两个部分初始化，一部分是新建（new）（构造函数），一部分是定义位置和属性（init函数）
    public loginView() {
        // 新建一个画布，并且带一个匿名内部类
        loginWin = new JPanel() {
            // 定义一张图片，新建一个ImageIcon对象并调用getImage方法获得一个Image对象
            String bgPath = "images/loginView/background.png";
            private Image image = new ImageIcon(ClassLoader.getSystemResource(bgPath)/*图片的路径*/).getImage();

            // 这里系统要调用这个paintComponent方法来画这张图片，这里系统传入了一个Graphics对象（画笔），
            // 我们需要用这个对象来画背景图片
            protected void paintComponent(Graphics g) {
                // 调用画笔的drawImage方法，参数是要画的图片，初始坐标，结束坐标，和在哪里画
                // this代表是LoginWin这个“画布”对象
                g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this);
            }
        };
        // 新建一个label，传入初始参数，暨要显示的内容，后面也可用setText方法来更新
        loginTitle = new JLabel("图 书 管 理 系 统");
        loginName = new JLabel("账号：");
        loginPWD = new JLabel("密码：");
        loginButton = new JButton("登录");
        helpButton = new JButton("帮助");
        usernameField = new MyComboBox<String>();
        passwordField = new MyPasswordField();

        System.out.println("新建窗口成功！");
        // 调用初始化函数，这里包括设置坐标、大小、设置监听方法
        intiView();
    }

    // 这里需要设置为私有的，以防止其他程序调用多次产生多个监听器而产生点一次按钮响应两次的结果
    private void intiView() {
        // 设置标题，this表示LoginView这个界面，所以里的标题是界面的标题
        this.setTitle("图书管理系统 - 用户登录");
        // 设置窗口可关闭，退出的方式有多种，exit和dispose
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // 设置窗口大小
        this.setSize(500, 350);
        // 禁用或启用此 frame 的装饰
        // this.setUndecorated(true);
        // 设置窗口位置，根据分辨率自动调整
        CenterView.CenterByWindow(this);
        // 不允许用户调整窗口大小
        this.setResizable(false);
        // 设置logo
        Utils.setLogo(this);

        // 设置容器布局方式为空布局
        loginWin.setLayout(null);
        // 默认是界面获得焦点
        loginWin.setFocusable(true);

        // 宽高，宽高
        // 通过setFont方法来设置标签的字体，包括大小
        loginTitle.setFont(new Font("微软雅黑", 1, 27)); // 3:斜体
        // setBounds是null的灵魂,设置坐标和大小，宽高，宽高
        loginTitle.setBounds(150, 30, 220, 30); // 居中

        loginName.setFont(new Font("微软雅黑", 0, 17));
        loginName.setBounds(108, 93, 60, 20);

        loginPWD.setFont(new Font("微软雅黑", 0, 17));
        loginPWD.setBounds(108, 133, 60, 20);

        // 登录按钮
        loginButton.setBounds(178, 190, 140, 40);
        loginButton.setFont(new Font("微软雅黑", 0, 14));

        // 帮助按钮
        helpButton.setFont(new Font("微软雅黑", 0, 12));
        helpButton.setBounds(218, 260, 60, 30);


        // 用来设置没有输入的时候的占位符
        usernameField.setBounds(168, 90, 200, 30);
        // 设置字体和 loginUserHistory 一致
        usernameField.setFont(new Font("SimSun", Font.PLAIN, 17));
        usernameField.setPlaceholder("请输入账号");//当输入框没有内容时,占位显示信息
        // 定义一个串数组，给下拉框的内容
        // 也可以通过.addItem("")的方法来设置
        String[] select = {"admin", "14122472"};
        usernameField.setModel(new DefaultComboBoxModel<String>(select));
        // 设置默认为空
        usernameField.setSelectedIndex(-1);
        // 设置下拉可编辑
        usernameField.setEditable(true);

        // 和上面一样
        passwordField.setBounds(168, 130, 200, 30);
        passwordField.setPlaceholder("请输入密码");


        // 增加按钮监听，监听类自己设计
        helpButton.addActionListener(new LoginView_helpButton_ActionListener(this));
        loginButton.addActionListener(new LoginView_loginButton_ActionListener(this));

        // 密码输入框回车触发登录事件
        passwordField.addActionListener(new LoginView_loginButton_ActionListener(this));

        // 把设置好的控件全加到画布里，这里注意一点顺序，先加的在上面，所以
        // 先加账号框再加下拉框
        loginWin.add(loginTitle);
        loginWin.add(loginName);
        loginWin.add(usernameField);
        loginWin.add(loginPWD);
        loginWin.add(passwordField);
        loginWin.add(loginButton);
        loginWin.add(helpButton);

        // 去掉按钮文字周围的焦点框
        loginButton.setFocusPainted(false);
        helpButton.setFocusPainted(false);

        // 把画布放到窗口里
        this.add(loginWin);
        // 窗口使能
        this.setVisible(true);
        // System.out.println("初始化窗口成功！");

    }

    // 下面两个是给登录监听类放出的端口
    public JPasswordField getPasswordField() {
        return passwordField;
    }

    public JTextField getLoginUser() {
        JTextField component = (JTextField) usernameField.getEditor().getEditorComponent();
        return component;
    }

}
