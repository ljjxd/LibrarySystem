package com.lijinjiang.view;

import com.lijinjiang.entity.User;
import com.lijinjiang.listener.*;
import com.lijinjiang.util.Utils;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.WindowEvent;

/**
 * 主界面
 */
@SuppressWarnings("serial")
public class MainView extends JFrame {
    private User user;
    private JPanel mainWin;
    private JLabel status;
    private JLabel userName;
    private JLabel avatar;
    private JLabel titleBack;
    private JLabel button1;
    private JLabel button2;
    private JLabel button3;
    private JLabel button4;
    private JLabel button5;
    private JLabel exit;

    /**
     * 主界面
     * user 登录上来的用户
     */
    public MainView(User user) {
        this.user = user;
        mainWin = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                // TODO Auto-generated method stub
                // 加载背景图片
                String bgPath = "images/MainView/background.png";
                Image image = new ImageIcon(ClassLoader.getSystemResource(bgPath)/*图片的路径*/).getImage();
                g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this);
            }
        };

        userName = new JLabel();
        avatar = new JLabel();
        titleBack = new JLabel();
        button1 = new JLabel("借阅管理");
        button2 = new JLabel("图书管理");
        // 用户管理
        button3 = new JLabel("用户管理");
        button4 = new JLabel("系统管理");
        button5 = new JLabel("图书管理");
        status = new JLabel();

        exit = new JLabel();

        Init();
    }

    private void Init() {
        // 设置标题
        this.setTitle("图书管理系统");
        // 设置窗口大小
        this.setSize(1102, 679);
        // 注册关闭事件
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // 窗口居中
        CenterView.CenterByWindow(this);
        // 设置容器布局方式为空布局
        mainWin.setLayout(null);
        // 不允许用户调整窗口大小
        this.setResizable(false);
        // 设置logo
        Utils.setLogo(this);


        // 设置用户名参数
        userName.setBounds(70, 0, 600, 30);
        userName.setFont(new Font("微软雅黑", 1, 27));

        status.setBounds(70, 40, 100, 30);
        status.setFont(new Font("微软雅黑", 3, 17));

        // 显示用户名称
        userName.setText(user.getName());
        // 显示是否管理员
        if (user.getAdmin()) {
            status.setText("管理员");
        } else {
            status.setText("普通用户");
            button2.setVisible(false);
        }

        // 设置头像参数
        setJLabelImageAndSize(5, 5, 60, 60, avatar, user.getAvatarSrc());
        // 创建蚀刻式边框
        avatar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        // 退出登录
        setJLabelImageAndSize(910, 10, 170, 57, exit, "images/MainView/exit/exit1.png");
        // 设置名片背景参数
        setJLabelImageAndSize(0, 0, 1098, 80, titleBack, "images/MainView/titleBackground.png");
        // 图书借阅
        setJLabelImageAndSize(70, 120, 275, 210, button1, "images/MainView/1/default.png");
        // 图书归还
        setJLabelImageAndSize(70, 350, 275, 210, button2, "images/MainView/2/default.png");
        // 用户管理
        setJLabelImageAndSize(415, 120, 275, 210, button3, "images/MainView/3/default.png");
        // 系统管理
        setJLabelImageAndSize(415, 350, 275, 210, button4, "images/MainView/4/default.png");
        // 图书管理
        setJLabelImageAndSize(760, 120, 275, 440, button5, "images/MainView/5/default.png");


        // 退出按钮
        avatar.addMouseListener(new MainView_avatar_MouseListener(this, user));
        exit.addMouseListener(new MainView_exit_MouseListener(this, exit));
        button1.addMouseListener(new MainView_Button1_MouseListener(this));
        button2.addMouseListener(new MainView_Button2_MouseListener(this));
        button3.addMouseListener(new MainView_Button3_MouseListener(this));
        button4.addMouseListener(new MainView_Button4_MouseListener(this));
        button5.addMouseListener(new MainView_Button5_MouseListener(this));


        mainWin.add(status);
        mainWin.add(avatar);
        mainWin.add(userName);
        mainWin.add(button1);
        mainWin.add(button2);
        mainWin.add(button3);
        mainWin.add(button4);
        mainWin.add(button5);
        mainWin.add(exit);

        mainWin.add(titleBack);

        this.add(mainWin);
        //设置窗口可见
        this.setVisible(true);
    }

    public void setJLabelImageAndSize(int x, int y, int width, int height, JLabel label, String imagePath) {
        // 实例化ImageIcon对象
        ImageIcon image = new ImageIcon(ClassLoader.getSystemResource(imagePath));
        // 得到Image对象
        Image img = image.getImage();
        // 创建缩放版本
        img = img.getScaledInstance(width, height, Image.SCALE_DEFAULT);
        // 替换为缩放版本
        image.setImage(img);
        // JLabel设置图像
        label.setIcon(image);
        // JLabel设置坐标和大小
        label.setBounds(x, y, width, height);
    }

    /**
     * 重写窗口的事件中转方法，程序是从这个方法processWindowEvent进入到窗口关闭事件的
     */
    @Override
    protected void processWindowEvent(WindowEvent e) {
        // 这里需要对进来的WindowEvent进行判断，因为，不仅会有窗口关闭的WindowEvent进来，还可能有其他的WindowEvent进来
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            int option = JOptionPane.showConfirmDialog(null, "是否关闭系统？", "提示", JOptionPane.OK_CANCEL_OPTION);
            if (option == JOptionPane.OK_OPTION) {
                // 用户选择关闭程序，以上代码提示后确认传给父类处理
                super.processWindowEvent(e);
            } else {
                //用户选择不退出程序，这里把关闭事件截取
                return;
            }
        } else {
            // 如果是其他事件，交给父类处理
            super.processWindowEvent(e);
        }
    }

    // 获取头像
    public JLabel getAvatar() {
        return avatar;
    }

    // 获取Button1
    public JLabel getButton1() {
        return button1;
    }

    // 获取Button2
    public JLabel getButton2() {
        return button2;
    }

    // 获取Button3
    public JLabel getButton3() {
        return button3;
    }

    // 获取Button4
    public JLabel getButton4() {
        return button4;
    }

    // 获取Button5
    public JLabel getButton5() {
        return button5;
    }




















	/*public void updateAdmin(AdministratorBeans admin){
		this.admin = admin;
		userName.setText(admin.getuserName());
		if(admin.isIfsuper()){
			status.setText("超级管理员");
		}else{
			status.setText("管理员");
		}
	}
	
	public boolean isSu(){
		return this.admin.isIfsuper();
	}
	
	public String getAdminAcc(){
		return this.admin.getAccount();
	}*/

    public String getPassword() {
        return this.user.getPassword();
    }

    // 获取当前登录用户
    public User getUser() {
        return this.user;
    }
}