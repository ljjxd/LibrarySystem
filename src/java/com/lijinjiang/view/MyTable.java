package com.lijinjiang.view;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.Enumeration;

/**
 * 自定义的一个JTable类，添加了标题居中等自适应方法
 */
public class MyTable extends JTable {
    private static final long serialVersionUID = 1423342345324L;

    // 重写JTable类的构造方法
    public MyTable() {
        super();//调用父类的构造方法

    }

    // 重写JTable类的构造方法，可以初始化行高
    public MyTable(int rowHeight) {//Vector rowData,Vector columnNames
        super();//调用父类的构造方法
        // 设置table的行高
        this.setRowHeight(rowHeight);

    }

    // 自定义列居中和最大宽度

    /**
     * @param columnName 列名
     */
    public void customColumn(String columnName) {
        DefaultTableCellRenderer r = new DefaultTableCellRenderer();
        r.setHorizontalAlignment(JTextField.CENTER);
        // 设置该列居中显示
        this.getColumn(columnName).setCellRenderer(r);
        // this.getColumn(columnName).setMinWidth(80);
        // 设置该列最大宽度
        this.getColumn(columnName).setMaxWidth(80);
    }

    /**
     * 重写JTable类的getTableHeader()方法
     * 设置表盒标题居中
     */
    public JTableHeader getTableHeader() {//定义表格头
        JTableHeader tableHeader = super.getTableHeader();//获得表格头对象
        tableHeader.setPreferredSize(new Dimension(100, 30)); // 设置表头行高
        tableHeader.setFont(new Font("微软雅黑", 1, 14));
        tableHeader.setReorderingAllowed(false);//设置表格列不可重排
        DefaultTableCellRenderer hr = (DefaultTableCellRenderer) tableHeader.getDefaultRenderer();//获得表格头的单元格对象
        hr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);//设置列名居中显示
        return tableHeader;
    }

    /**
     * 重写JTable类的getDefaultRenderer(Class<?>columnClass)方法
     * 设置表格数据居中
     *
     * @param columnClass
     * @return
     */
    public TableCellRenderer getDCellRenderer(Class<?> columnClass) {//定义单元格
        DefaultTableCellRenderer cr = (DefaultTableCellRenderer) super
                .getDefaultRenderer(columnClass);//获得表格的单元格对象
        cr.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);//设置单元格内容居中显示
        return cr;
    }

    /**
     * 重写JTable类的isCellEditable(int row,int column)方法
     * 禁止表格编辑
     */
    @Override
    public boolean isCellEditable(int row, int column) {//表格不可编辑
        return false;
    }

    //@SuppressWarnings("rawtypes")
    /**
     * 表头自适应函数，存数据后调
     * @param myTable
     */
    /*public void FitTableColumns() {
        JTableHeader header = this.getTableHeader();
        int rowCount = this.getRowCount();
        Enumeration columns = this.getColumnModel().getColumns();
        while (columns.hasMoreElements()) {
            TableColumn column = (TableColumn) columns.nextElement();
            int col = header.getColumnModel().getColumnIndex(column.getIdentifier());
            int width = (int) this.getTableHeader().getDefaultRenderer()
                    .getTableCellRendererComponent(this, column.getIdentifier()
                            , false, false, -1, col).getPreferredSize().getWidth();
            for (int row = 0; row < rowCount; row++) {
                int preferedWidth = (int) this.getCellRenderer(row, col).getTableCellRendererComponent(this,
                        this.getValueAt(row, col), false, false, row, col).getPreferredSize().getWidth();
                width = Math.max(width, preferedWidth);
            }
            header.setResizingColumn(column); // 此行很重要
            column.setWidth(width + this.getIntercellSpacing().width);
        }
    }*/

}