package com.lijinjiang.view;

import com.lijinjiang.entity.User;
import com.lijinjiang.listener.PersonalInfoView_ActionListener;

import javax.swing.*;
import java.awt.*;

/**
 * 个人信息页面
 */
@SuppressWarnings("serial")
public class PersonalInfoView extends JDialog {
    private JPanel PersonalWin; // 本窗口
    private int WindowsHeight; // 父窗口高度
    private int windowsWidth; // 父窗口宽度
    private JLabel avatarLabel; // 头像框
    private JButton changePwdBtn; // 点击更改密码
    private JButton closeBtn; // 关闭

    private JLabel nameLabel; // 姓名
    private JTextField nameField; // 姓名输入框
    private JLabel sexLabel; // 性别
    private JPanel sexPanel; // 性别面板
    private JRadioButton radioButton1, radioButton2, radioButton3;// 性别单选
    private ButtonGroup buttonGroup; // 按钮组
    private JLabel collegeLabel; // 学院
    private JTextField collegeField; // 学院输入框
    private JLabel phoneLabel; // 电话
    private JTextField phoneField; // 电话输入框

    User user;
    MainView mv;

    public PersonalInfoView(MainView mv, User user) {
        super(mv, "个人信息", true);
        this.user = user;
        this.mv = mv;
        PersonalWin = new JPanel() {
            // 定义一张图片，新建一个ImageIcon对象并调用getImage方法获得一个Image对象
            String path = "images/PersonalInfoView/background.png";
            private Image image = new ImageIcon(ClassLoader.getSystemResource(path)/*图片的路径*/).getImage();

            // 这里系统要调用这个paintComponent方法来画这张图片，这里系统传入了一个Graphics对象（画笔），
            // 我们需要用这个对象来画背景图片
            protected void paintComponent(Graphics g) {
                // 调用画笔的drawImage方法，参数是要画的图片，初始坐标，结束坐标，和在哪里画
                // this代表是LoginWin这个“画布”对象
                g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), this);
            }
        };

        WindowsHeight = mv.getHeight();
        windowsWidth = mv.getWidth();
        // 头像部分
        avatarLabel = new JLabel();

        // 姓名部分
        nameLabel = new JLabel("姓名：");
        nameField = new JTextField(user.getName());
        nameField.setEnabled(false);
        // 性别部分
        sexLabel = new JLabel("性别：");
        sexPanel = new JPanel();
        sexPanel.setLayout(new GridLayout(1, 3));
        radioButton1 = new JRadioButton("未知");
        radioButton2 = new JRadioButton("男");
        radioButton3 = new JRadioButton("女");
        buttonGroup = new ButtonGroup();
        buttonGroup.add(radioButton1);
        buttonGroup.add(radioButton2);
        buttonGroup.add(radioButton3);
        if (user.getSex() == 0) {
            radioButton1.setSelected(true);
        } else if (user.getSex() == 1) {
            radioButton2.setSelected(true);
        } else if (user.getSex() == 2) {
            radioButton3.setSelected(true);
        }
        radioButton1.setEnabled(false);
        radioButton2.setEnabled(false);
        radioButton3.setEnabled(false);
        sexPanel.add(radioButton1);
        sexPanel.add(radioButton2);
        sexPanel.add(radioButton3);
        // 学院部分
        collegeLabel = new JLabel("学院：");
        collegeField = new JTextField(user.getCollege());
        collegeField.setEnabled(false);
        // 电话部分
        phoneLabel = new JLabel("电话：");
        phoneField = new JTextField(user.getPhone());
        phoneField.setEnabled(false);


        // 按钮部分
        closeBtn = new JButton("关闭");
        changePwdBtn = new JButton("修改密码");

        Init();
    }

    private void Init() {
        // TODO Auto-generated method stub
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setSize(windowsWidth - 700, WindowsHeight - 200);
        PersonalWin.setLayout(null);
        PersonalWin.setFocusable(true);
        //不允许用户调整窗口大小
        this.setResizable(false);
        CenterView.CenterByWindow(this);

        // 头像
        avatarLabel.setBounds(108, 15, 185, 200);
        // 实例化ImageIcon 对象
        ImageIcon image = new ImageIcon(ClassLoader.getSystemResource(user.getAvatarSrc()));
        // 得到Image对象
        Image img = image.getImage();
        // 创建缩放版本
        img = img.getScaledInstance(165, 165, Image.SCALE_DEFAULT);
        // 替换为缩放版本
        image.setImage(img);
        // JLabel设置图像
        avatarLabel.setIcon(image);
        // 设置图像居中
        avatarLabel.setHorizontalAlignment(JLabel.CENTER);
        avatarLabel.setBorder(BorderFactory.createTitledBorder(user.getName()));
        // 姓名
        nameLabel.setFont(new Font("微软雅黑", 1, 17));
        nameLabel.setBounds(70, 235, 60, 30);
        nameField.setBounds(130, 235, 190, 30);
        // 性别
        sexLabel.setFont(new Font("微软雅黑", 1, 17));
        sexLabel.setBounds(70, 275, 60, 30);
        sexPanel.setBounds(130, 275, 190, 30);
        // 姓名
        collegeLabel.setFont(new Font("微软雅黑", 1, 17));
        collegeLabel.setBounds(70, 315, 60, 30);
        collegeField.setBounds(130, 315, 190, 30);
        // 姓名
        phoneLabel.setFont(new Font("微软雅黑", 1, 17));
        phoneLabel.setBounds(70, 355, 60, 30);
        phoneField.setBounds(130, 355, 190, 30);

        // 修改密码按钮
        changePwdBtn.setBounds(80, 405, 80, 30);
        // 关闭按钮
        closeBtn.setBounds(260, 405, 60, 30);


        PersonalInfoView_ActionListener listen = new PersonalInfoView_ActionListener(this, mv, user);

        changePwdBtn.addActionListener(listen);

        closeBtn.addActionListener(listen);

        // 添加头像
        PersonalWin.add(avatarLabel);
        // 添加个人信息
        PersonalWin.add(nameLabel);
        PersonalWin.add(nameField);
        PersonalWin.add(sexLabel);
        PersonalWin.add(sexPanel);
        PersonalWin.add(collegeLabel);
        PersonalWin.add(collegeField);
        PersonalWin.add(phoneLabel);
        PersonalWin.add(phoneField);

        // 添加按钮
        PersonalWin.add(changePwdBtn);
        PersonalWin.add(closeBtn);

        // 去掉按钮文字周围的焦点框
        changePwdBtn.setFocusPainted(false);
        closeBtn.setFocusPainted(false);

        this.add(PersonalWin);
        this.setVisible(true);
    }

    // 获取修改密码按钮
    public JButton getChangePwdBtn() {
        return changePwdBtn;
    }

    // 获取关闭按钮
    public JButton getCloseBtn() {
        return closeBtn;
    }

}
