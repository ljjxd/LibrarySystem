package com.lijinjiang.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 自定义提示框
 */
public class messageDialog extends JDialog {
    // 计时器设置为500ms
    Timer timer = new Timer(100, new ActionListener() {
        int p = 5;
        @Override
        public void actionPerformed(ActionEvent e) {
            p--;
            if (p < 0) {
                // 停止计时器
                timer.stop();
                // 提示框自我销毁
                messageDialog.this.dispose();
            }
        }
    });

    public messageDialog(Frame owner, String message,Dimension dimension) {
        super(owner,"提示",true);
        // 设置大小
        this.setSize(dimension);
        // 设置居中显示
        this.setLocationRelativeTo(null);
        // 添加消息内容
        JPanel panel = new JPanel();
        JLabel messageLabel = new JLabel(message);
        panel.add(messageLabel);
        this.setContentPane(panel);
        // 设置大小不可变
        this.setResizable(false);
        // 启动计时器
        timer.start();
    }

}
