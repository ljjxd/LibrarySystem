package com.lijinjiang.view;

import javax.swing.*;
import java.awt.*;

/**
 * 一个组合类，组合标签和输入框，或者两个标签，这样更方便创建
 */
@SuppressWarnings("serial")
public class ComboLabelField extends JPanel {
    private JLabel attribute; // 属性标签
    private JTextField valueField; // 属性值


    /**
     * @param text     属性
     * @param readOnly 是否只读
     */
    public ComboLabelField(String text, boolean readOnly) {
        attribute = new JLabel(text);
        this.setLayout(new FlowLayout());

        this.add(attribute);
        valueField = new JTextField(20);
        valueField.setPreferredSize(new Dimension(120, 28));
        if (readOnly) {
            this.add(valueField);
            valueField.setEditable(false);
        } else {
            this.add(valueField);
        }
    }


    /**
     * @param text     属性
     * @param readOnly 是否只读
     * @param width    valueField的宽度
     */
    public ComboLabelField(String text, boolean readOnly, int width) {
        attribute = new JLabel(text);
        this.setLayout(new FlowLayout());

        this.add(attribute);
        valueField = new JTextField(width);
        valueField.setPreferredSize(new Dimension(width, 28));
        if (readOnly) {
            this.add(valueField);
            valueField.setEditable(false);
        } else {
            this.add(valueField);
        }
    }

    public void setAttributeText(String text) {
        this.attribute.setText(text);
    }

    public void setValueText(String text) {
        this.valueField.setText(text);
    }

    public String getText() {
        if (this.valueField.getText() == null) {
            return "";
        }
        return this.valueField.getText();
    }

	/*public void setRatio(int ratio){
		int JpW = this.getWidth();
		int JpH = this.getHeight();
		this.setLayout(null);
		if(ratio>JpW){
			return;
		}
		attribute.setBounds(0, 0, ratio, JpH);
		valueField.setBounds(ratio, 0, JpW - ratio, JpH);
	}*/

    public JLabel getAttribute() {
        return attribute;
    }

    public JTextField getValueField() {
        return this.valueField;
    }

}