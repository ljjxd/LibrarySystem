package com.lijinjiang.view;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * 帮助界面
 */
public class helpView extends JDialog {
    private static final long serialVersionUID = 1L;
    private boolean refreshTime = true;
    private JPanel helpWin;
    private JLabel explainField;
    private JTextField producerField;
    private JTextArea thankField;
    private JScrollPane scrollPane;
    private JLabel dateLabel;

    public helpView(loginView lv) {
        //设置父窗口标题并且禁止操作父视图
        super(lv, "帮助", true);
        helpWin = new JPanel();
        explainField = new JLabel("做这个项目的主要原因是原UI太好看了 - 颜狗石锤");
        producerField = new JTextField();
        thankField = new JTextArea();
        scrollPane = new JScrollPane();
        dateLabel = new JLabel();
        this.setResizable(false);
        Init();
    }

    private void Init() {
        //启用窗口关闭
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        //设置窗口大小
        this.setSize(400, 340);
        //设置窗口居中
        CenterView.CenterByWindow(this);
        //设置容器布局方式为空布局
        helpWin.setLayout(null);

        explainField.setBounds(10, 10, 370, 50);
        explainField.setBorder(BorderFactory.createTitledBorder("说明"));

        // 制作组
        producerField.setBounds(10, 70, 370, 50);
        producerField.setEditable(false);
        producerField.setBorder(BorderFactory.createTitledBorder("制作"));
        producerField.setText("制作组成员：1123GY");

        // 致谢
        thankField.setBounds(10, 130, 370, 100);
        thankField.setBackground(new Color(240, 240, 240)); // 设置背景色
        thankField.setLineWrap(true); // 设置自动换行
        thankField.setEditable(false);
        thankField.setBorder(BorderFactory.createTitledBorder("致谢"));
        thankField.setText("感谢[鱼啊鱼]在Gitee上开源的项目[BlueRabbitLibrarySystem]\r\n" +
                "项目地址：https://gitee.com/minuy/BlueRabbitLibrarySystem");

        // 时间
        dateLabel.setBounds(10, 240, 370, 50);
        dateLabel.setBorder(BorderFactory.createTitledBorder("时间"));
        dateLabel.setText(getStringDate(new Date()));

        helpWin.add(dateLabel);
        helpWin.add(explainField);
        helpWin.add(thankField);
        helpWin.add(producerField);

        //设置主视图的大小
        helpWin.setPreferredSize(new Dimension(370, 300));


        //添加滑动条
        scrollPane.setViewportView(helpWin);
        scrollPane.setBounds(0, 0, 380, 330);

        this.getContentPane().add(scrollPane);
        showTime();
        this.setVisible(true);

        refreshTime = false;
    }


    // 将时间转换成String类型返回
    private String getStringDate(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒");
        String stringDate = dateFormat.format(date);
        return stringDate;
    }

    // 在时间框内显示当前时间
    private void showTime() {
        new Thread() {
            @Override
            public void run() {
                while (refreshTime) {
                    try {
                        sleep(1000);
                        Date now = new Date();
                        dateLabel.setText(getStringDate(now));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
