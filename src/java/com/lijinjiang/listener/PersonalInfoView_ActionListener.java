package com.lijinjiang.listener;

import com.lijinjiang.entity.User;
import com.lijinjiang.view.ChangePwdView;
import com.lijinjiang.view.MainView;
import com.lijinjiang.view.PersonalInfoView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 个人信息详情页面的监听类，监听整个页面
 */
public class PersonalInfoView_ActionListener implements ActionListener {
    PersonalInfoView piv;
    User user;
    MainView mv;

    public PersonalInfoView_ActionListener(PersonalInfoView personalInfoView,
                                           User user) {
        // TODO Auto-generated constructor stub
    }

    public PersonalInfoView_ActionListener(PersonalInfoView personalInfoView,
                                           MainView mv, User user) {
        // TODO Auto-generated constructor stub
        this.piv = personalInfoView;
        this.user = user;
        this.mv = mv;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        Object event = e.getSource();
        System.out.println(e);
        // 点击更改密码按钮
        if (event == piv.getChangePwdBtn()) {
            System.out.println("更改密码");
            // 实例化一个修改密码页面，需要输入旧密码
            new ChangePwdView(piv, user, true);
        } else if (event == piv.getCloseBtn()) {
            // 点击关闭按钮
            System.out.println("关闭");
            piv.dispose();
        }
    }
}

