package com.lijinjiang.listener;

import com.lijinjiang.view.helpView;
import com.lijinjiang.view.loginView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 登录界面帮助按钮的监听类
 * @author minuy
 *
 */
public class LoginView_helpButton_ActionListener implements ActionListener {
	private loginView lv;//对窗口的引用
	
	public LoginView_helpButton_ActionListener(loginView lv) {
		this.lv = lv;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		//lv.dispose();
		new helpView(lv);
	}

}
