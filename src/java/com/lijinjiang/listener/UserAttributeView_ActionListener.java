package com.lijinjiang.listener;

import com.lijinjiang.entity.User;
import com.lijinjiang.service.UserService;
import com.lijinjiang.view.UserAttributeView;
import com.lijinjiang.view.UserManageView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 添加用户页面详情页面的监听类，监听所有这个页面的按钮点击事件
 */
public class UserAttributeView_ActionListener implements ActionListener {
    UserAttributeView attributeView;
    UserManageView manageView;

    public UserAttributeView_ActionListener(
            UserAttributeView readerInfoUpdate, UserManageView manageView) {
        // TODO Auto-generated constructor stub
        this.attributeView = readerInfoUpdate;
        this.manageView = manageView;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        Object event = e.getSource();
        UserService userService = new UserService();
        if (event == attributeView.getSaveBtn()) {
            if (attributeView.isAddModel()) {
                // 新增
                userService.addUser(manageView, attributeView);
            } else {
                // 修改
                userService.updateUserInfo(manageView, attributeView);
            }

        } else {
            if (event == attributeView.getCancelBtn()) {
                // System.out.println("取消");
                attributeView.dispose();
            }
        }
    }

}