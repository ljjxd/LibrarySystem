package com.lijinjiang.listener;

import com.lijinjiang.view.MainView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * 主视图图书管理按钮的监听类
 */
public class MainView_Button5_MouseListener implements MouseListener {

	JLabel label;
	MainView mv;

	public MainView_Button5_MouseListener(MainView button5) {
		// TODO Auto-generated constructor stub
		this.label = button5.getButton5();
		this.mv = button5;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("进入系统管理！");
		// new UserInfoManageView(mv);
	}

	//鼠标点着不放开
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(760, 120, 275, 440, label, "images/MainView/5/press.png");
	}

	//鼠标点击放开了
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(760, 120, 275, 440, label, "images/MainView/5/focus.png");
	}

	//鼠标到上面了
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(760, 120, 275, 440, label, "images/MainView/5/focus.png");
	}

	//鼠标离开了
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(760, 120, 275, 440, label, "images/MainView/5/default.png");
	}

}