package com.lijinjiang.listener;

import com.lijinjiang.view.MainView;
import com.lijinjiang.view.UserManageView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * 主视图用户管理按钮的监听类
 */
public class MainView_Button3_MouseListener implements MouseListener {

	JLabel label;
	MainView mv;
	
	public MainView_Button3_MouseListener(MainView button3) {
		// TODO Auto-generated constructor stub
		this.label = button3.getButton3();
		this.mv = button3;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("进入用户管理！");
		new UserManageView(mv);

	}

	//鼠标点着不放开
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 120, 275, 210, label, "images/MainView/3/press.png");
	}

	//鼠标点击放开了
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 120, 275, 210, label, "images/MainView/3/focus.png");
	}

	//鼠标到上面了
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 120, 275, 210, label, "images/MainView/3/focus.png");
	}

	//鼠标离开了
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 120, 275, 210, label, "images/MainView/3/default.png");
	}

}