package com.lijinjiang.listener;

import com.lijinjiang.entity.User;
import com.lijinjiang.view.MainView;
import com.lijinjiang.view.PersonalInfoView;
import com.lijinjiang.view.SetButtonState;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * 主视图个人信息详情页面的头像点击按钮监听类
 */
@SuppressWarnings("serial")
public class MainView_avatar_MouseListener extends JDialog implements
        MouseListener {

    MainView mv;
    User user;
    JLabel avatar;

    public MainView_avatar_MouseListener(MainView mainView, User user) {
        // TODO Auto-generated constructor stub
        this.mv = mainView;
        this.user = user;
        this.avatar = mv.getAvatar();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        new PersonalInfoView(mv, user);
    }

    // 鼠标到上面了
    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
        SetButtonState.setButtonUp(avatar);
    }

    // 鼠标点着不放开
    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        SetButtonState.setButtonDown(avatar);
    }

    // 鼠标点击放开了
    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        SetButtonState.setButtonUp(avatar);
    }


    // 鼠标离开了
    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        // SetButtonState.reSetButton(avatar);
        avatar.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED)); // 恢复蚀刻式边框
    }

}
