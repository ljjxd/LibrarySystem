package com.lijinjiang.listener;

import com.lijinjiang.view.MainView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * 主视图图书借阅按钮的监听类
 */
public class MainView_Button1_MouseListener implements MouseListener {

	JLabel label;
	MainView mv;

	public MainView_Button1_MouseListener(MainView button1) {
		// TODO Auto-generated constructor stub
		this.label = button1.getButton1();
		this.mv = button1;
	}

	// 按下并松开鼠标按钮时发生
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("进入图书借阅界面！");
		// new LibraryManagementSelectView(mv);

	}

	// 鼠标点着不放开
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 120, 275, 210, label, "images/MainView/1/press.png");
	}

	// 鼠标点击放开了
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 120, 275, 210, label, "images/MainView/1/focus.png");
	}

	// 鼠标到上面了
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 120, 275, 210, label, "images/MainView/1/focus.png");
	}

	// 鼠标离开了
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 120, 275, 210, label, "images/MainView/1/default.png");
	}

}