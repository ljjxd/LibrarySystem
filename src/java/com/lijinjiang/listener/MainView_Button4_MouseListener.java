package com.lijinjiang.listener;

import com.lijinjiang.view.MainView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * 主视图系统管理按钮的监听类
 */
public class MainView_Button4_MouseListener implements MouseListener {

	JLabel label;
	MainView mv;

	public MainView_Button4_MouseListener(MainView button4) {
		// TODO Auto-generated constructor stub
		this.label = button4.getButton4();
		this.mv = button4;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("进入系统管理！");
		// new UserInfoManageView(mv);
	}

	//鼠标点着不放开
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 350, 275, 210, label, "images/MainView/4/press.png");
	}

	//鼠标点击放开了
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 350, 275, 210, label, "images/MainView/4/focus.png");
	}

	//鼠标到上面了
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 350, 275, 210, label, "images/MainView/4/focus.png");
	}

	//鼠标离开了
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(415, 350, 275, 210, label, "images/MainView/4/default.png");
	}

}