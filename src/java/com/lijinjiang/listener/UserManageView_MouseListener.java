package com.lijinjiang.listener;

import com.lijinjiang.service.UserService;
import com.lijinjiang.view.ChangePwdView;
import com.lijinjiang.view.SetButtonState;
import com.lijinjiang.view.UserAttributeView;
import com.lijinjiang.view.UserManageView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;

/**
 * 用户管理界面的监听类，用来监听用户管理界面的所有按钮和操作
 */
public class UserManageView_MouseListener implements MouseListener {
    UserManageView userManageView;

    public UserManageView_MouseListener(
            UserManageView UserManageView) {
        // TODO Auto-generated constructor stub
        this.userManageView = UserManageView;
    }

    // 按下并松开鼠标按钮时发生
    @Override
    public void mouseClicked(MouseEvent e) {
        // TODO Auto-generated method stub
        Object clicked = e.getSource();
        UserService userService = new UserService();
        if (clicked == userManageView.getAddBtn()) {
            System.out.println("新增用户！");
            // 打开添加用户界面
            new UserAttributeView(userManageView, "新增用户信息", null, false);
        } else if (clicked == userManageView.getModifyBtn()) {
            System.out.println("修改用户信息！");
            //打开修改窗口
            userService.modifyUserInfo(userManageView);
        } else if (clicked == userManageView.getDeleteBtn()) {
            System.out.println("删除用户！");
            userService.deleteUser(userManageView);
        } else if (clicked == userManageView.getResetPwdBtn()) {
            System.out.println("重置密码！");
            userService.resetUserPwd(userManageView);
        } else if (clicked == userManageView.getCloseBtn()) {
            System.out.println("关闭用户管理！");
            userManageView.dispose();
        } else if (clicked == userManageView.getSearchBtn()) {
            System.out.println("搜索用户");
            userService.getSearchUser(userManageView);
        } else if (clicked == userManageView.getTableDataView()) {
            if (e.getClickCount() == 2) {
                System.out.println("查看用户信息！");
                userService.viewUserInfo(userManageView);
            }
        }
    }

    // 鼠标到上面了
    @Override
    public void mouseEntered(MouseEvent e) {
        Object clicked = e.getSource();
        if (clickedButton(clicked)) {
            SetButtonState.setButtonUp((JLabel) clicked);
        }
    }


    // 鼠标点着不放开
    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        Object clicked = e.getSource();
        if (clickedButton(clicked)) {
            SetButtonState.setButtonDown((JLabel) clicked);
        }
    }


    // 鼠标点击放开了
    @Override
    public void mouseReleased(MouseEvent e) {
        Object clicked = e.getSource();
        if (clickedButton(clicked)) {
            SetButtonState.setButtonUp((JLabel) clicked);
        }
    }


    // 鼠标离开了
    @Override
    public void mouseExited(MouseEvent e) {
        // TODO Auto-generated method stub
        Object clicked = e.getSource();
        if (clickedButton(clicked)) {
            SetButtonState.reSetButton((JLabel) clicked);
        }
    }

    // 判断是不是点击的按钮
    private boolean clickedButton(Object clicked) {
        // 新增按钮、修改按钮、删除按钮、重置密码按钮、关闭按钮、查询按钮
        if (clicked == userManageView.getAddBtn()
                || clicked == userManageView.getModifyBtn()
                || clicked == userManageView.getDeleteBtn()
                || clicked == userManageView.getResetPwdBtn()
                || clicked == userManageView.getCloseBtn()
                || clicked == userManageView.getSearchBtn()) {
            return true;
        }
        return false;
    }
}