package com.lijinjiang.listener;

import com.lijinjiang.view.MainView;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * 主视图图书归还按钮的监听类
 */
public class MainView_Button2_MouseListener implements MouseListener {

	JLabel label;
	MainView mv;

	public MainView_Button2_MouseListener(MainView button2) {
		// TODO Auto-generated constructor stub
		this.label = button2.getButton2();
		this.mv = button2;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		System.out.println("进入图书归还界面！");
		// new LibraryManagementSelectView(mv);

	}

	// 鼠标点着不放开
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 350, 275, 210, label, "images/MainView/2/press.png");
	}

	// 鼠标点击放开了
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 350, 275, 210, label, "images/MainView/2/focus.png");
	}

	// 鼠标到上面了
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 350, 275, 210, label, "images/MainView/2/focus.png");
	}

	// 鼠标离开了
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		mv.setJLabelImageAndSize(70, 350, 275, 210, label, "images/MainView/2/default.png");
	}

}