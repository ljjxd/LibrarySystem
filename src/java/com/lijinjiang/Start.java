package com.lijinjiang;

import javax.swing.UIManager;

import com.lijinjiang.view.loginView;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.FrameBorderStyle;

/**
 * 主函数，设置皮肤，启动窗口
 */
public class Start {

    public static void main(String[] args) {
        try {
            // 让swing界面变得和当前系统一样
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println(e);
        }
        new loginView();
    }
}