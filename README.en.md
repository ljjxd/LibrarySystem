# LibrarySystem

#### Description
近年来，随着社会、经济的发展，和人对书籍的需求的增长，许多图书馆的规模都在不断扩大，图书数量、有关图书的各种信息量、来图书馆的读者也成倍增加。巨大的信息量，使传统的人工式管理方法面临着极大的挑战，按照传统的方式管理相关信息，会导致图书馆管理上的混乱，人力与物力过多浪费，信息的准确性和安全性也禁不起推敲。图书馆混乱的管理方式，会使图书馆的负担过重，影响整个图书馆的运作和控制管理。
好在IT行业和Internet的飞速发展，为图书馆的管理方式带来了革新。一套合理、有效，规范和实用的图书管理系统，对图书资料和用户信息进行集中统一的管理。另一方面，图书馆的客人也可以通过该系统查询书籍，自助借书还书，节省了大量的人力物力，实现了信息的自动化处理，提高了处理的及时性和正确性。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
